bitseq (0.7.5+dfsg-7) UNRELEASED; urgency=medium

  * Remove Tim Booth <tbooth@ceh.ac.uk> since address is bouncing
    (Thank you for your work on this package, Tim)

 -- Andreas Tille <tille@debian.org>  Mon, 16 Sep 2024 09:59:37 +0200

bitseq (0.7.5+dfsg-6) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 02 Sep 2021 06:56:19 +0200

bitseq (0.7.5+dfsg-5) unstable; urgency=medium

  * Use 2to3 to port scripts to Python3
    Closes: #936209
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Sun, 01 Sep 2019 07:54:49 +0200

bitseq (0.7.5+dfsg-4) unstable; urgency=medium

  [ Graham Inggs ]
  * Link -lbam before -lz to fix build with -Wl,--as-needed

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Drop useless get-orig-source target

 -- Andreas Tille <tille@debian.org>  Fri, 21 Dec 2018 14:56:23 +0100

bitseq (0.7.5+dfsg-3) unstable; urgency=medium

  * Build-Depends: python-numpy (Thanks for the patch to Chris Lamb
    <lamby@debian.org>)
    Closes: #884677

 -- Andreas Tille <tille@debian.org>  Mon, 18 Dec 2017 12:12:21 +0100

bitseq (0.7.5+dfsg-2) unstable; urgency=medium

  * Drop -mtune=generic from build options
    Closes: #884623
  * Standards-Version: 4.1.2

 -- Andreas Tille <tille@debian.org>  Sun, 17 Dec 2017 21:13:47 +0100

bitseq (0.7.5+dfsg-1) unstable; urgency=medium

  * Initial upload to Debian (Closes: #883322)

 -- Andreas Tille <tille@debian.org>  Sat, 02 Dec 2017 12:41:04 +0100

bitseq (0.4.3-0ubuntu2) precise; urgency=low

  * Initial release.
  * Build against stock libbam (only static just now)
  * Build against stock libboost
  * For now, leave the names of the binaries as-is

 -- Tim Booth <tbooth@ceh.ac.uk>  Mon, 11 Feb 2013 09:59:05 +0000
