Source: bitseq
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               zlib1g-dev,
               libbam-dev,
               libboost-dev,
               help2man,
               python3,
               python3-numpy
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/bitseq
Vcs-Git: https://salsa.debian.org/med-team/bitseq.git
Homepage: https://github.com/BitSeq/BitSeq
Rules-Requires-Root: no

Package: bitseq
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         python3,
         python3-numpy
Suggests: samtools
Description: Bayesian Inference of Transcripts from Sequencing Data
 BitSeq is an application for inferring expression levels of individual
 transcripts from sequencing (RNA-Seq) data and estimating differential
 expression (DE) between conditions. An advantage of this approach is the
 ability to account for both technical uncertainty and intrinsic biological
 variance in order to avoid false DE calls. The technical contribution to the
 uncertainty comes both from finite read-depth and the possibly ambiguous
 mapping of reads to multiple transcripts.
