Dependencies:
---------------------------------------
GNU make, g++ , zlib (for samtools API used in parseAlignment program)
Optional:
python - for helper scripts getCount.py, extractTranscriptInfo.py


Compilation:
--------------------------------------
Enter BitSeq directory and run:
make


Help & Usage:
--------------------------------
See wiki for more information:
https://github.com/BitSeq/BitSeq/wiki/Basic-usage


Contact:
----------------------------------
Please use local issue tracker: https://github.com/BitSeq/BitSeq/issues for 
help, issue resolution and comments.

For direct contact, please contact Peter Glaus (glaus [at] cs.man.ac.uk).


License:
----------------------------------
Artistic-2.0 
 + Boost_1_0 for directory boost 
 + MIT for directory samtools
 + LGPL for directory asa103

