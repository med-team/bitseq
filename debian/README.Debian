BitSeq for Debian
------------------

Dealing with file names featuring script language extensions
============================================================

Debian policy does not permit script language extensions and this
Wiki page
   https://wiki.debian.org/UpstreamGuide#Language_extensions_in_scripts
provides good reasons for it.

Assuming that some users might have some scripts that are relying on
this extension this package provides the following workaround:

  1. Scripts with extensions are in
        /usr/lib/debian-med/bin/
  2. Scripts without the extension are linked to
        /usr/bin

So if you want to use the original script name just do the
following:  Set the PATH variable like

  export PATH=/usr/lib/debian-med/bin:$PATH

This makes sure that scripts you are using will find the original script.

 -- Andreas Tille <tille@debian.org>  Wed, 01 Jan 2014 19:04:47 +0100
